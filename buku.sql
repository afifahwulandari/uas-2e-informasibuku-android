/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.1.37-MariaDB : Database - buku
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`buku` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `buku`;

/*Table structure for table `bukuinfo` */

DROP TABLE IF EXISTS `bukuinfo`;

CREATE TABLE `bukuinfo` (
  `id_buku` int(11) NOT NULL,
  `id_jenis` int(11) DEFAULT NULL,
  `judul_buku` varchar(50) DEFAULT NULL,
  `nm_pengarang` varchar(50) DEFAULT NULL,
  `harga` int(50) DEFAULT NULL,
  `photos` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_buku`),
  KEY `id_jenis` (`id_jenis`),
  CONSTRAINT `bukuinfo_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `bukuinfo` */

insert  into `bukuinfo`(`id_buku`,`id_jenis`,`judul_buku`,`nm_pengarang`,`harga`,`photos`) values 
(12345,3,'kisahku','dimdim',200000,'dim.jpg');

/*Table structure for table `jenis` */

DROP TABLE IF EXISTS `jenis`;

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nm_jenis` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `jenis` */

insert  into `jenis`(`id_jenis`,`nm_jenis`) values 
(1,'majalah'),
(3,'novel'),
(5,'huu');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(50) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id_user`,`username`,`password`) values 
(1,'afifahsanti','afifahsanti');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
