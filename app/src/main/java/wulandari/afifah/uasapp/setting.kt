package wulandari.afifah.uasapp

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_setting.*

class setting : AppCompatActivity(), View.OnClickListener {
    lateinit var pref : SharedPreferences
    var setwar =""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        pref = getSharedPreferences("a", Context.MODE_PRIVATE)
        setwar = pref.getString("bl","").toString()

        btnBlue.setOnClickListener(this)
        btnYellow.setOnClickListener(this)
        btnGreen.setOnClickListener(this)
        btnWhite.setOnClickListener(this)
        warna()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnBlue->{
                pref =getSharedPreferences("a", Context.MODE_PRIVATE)
                val warna = pref.edit()
                warna.putString("bl","biru")
                warna.commit()
                Toast.makeText(this," warna berhasil di ubah",Toast.LENGTH_LONG).show()
                val asa  = Intent(this,SplassActivity::class.java)
                startActivity(asa)
            }
            R.id.btnGreen->{
                pref =getSharedPreferences("a",Context.MODE_PRIVATE)
                val warna = pref.edit()
                warna.putString("bl","hijau")
                warna.commit()
                Toast.makeText(this," warna berhasil di ubah",Toast.LENGTH_LONG).show()
                val asa  = Intent(this,SplassActivity::class.java)
                startActivity(asa)
            }
            R.id.btnWhite->{
                pref =getSharedPreferences("a",Context.MODE_PRIVATE)
                val warna = pref.edit()
                warna.putString("bl","putih")
                warna.commit()
                Toast.makeText(this," warna berhasil di ubah",Toast.LENGTH_LONG).show()
                val asa  = Intent(this,SplassActivity::class.java)
                startActivity(asa)
            }
            R.id.btnYellow->{
                pref =getSharedPreferences("a",Context.MODE_PRIVATE)
                val warna = pref.edit()
                warna.putString("bl","kuning")
                warna.commit()
                Toast.makeText(this," warna berhasil di ubah",Toast.LENGTH_LONG).show()
                val asa  = Intent(this,SplassActivity::class.java)
                startActivity(asa)
            }

        }
    }

    @SuppressLint("ResourceAsColor")
    fun  warna(){
        if(setwar=="biru"){
            saya.setBackgroundColor(Color.CYAN)
        }
        else if(setwar=="kuning"){
            saya.setBackgroundColor(R.color.colorPrimary)
        }
        else if(setwar=="hijau"){
            saya.setBackgroundColor(Color.GREEN)
        }
        else if(setwar=="putih"){
            saya.setBackgroundColor(Color.BLACK)
        }


    }


}
