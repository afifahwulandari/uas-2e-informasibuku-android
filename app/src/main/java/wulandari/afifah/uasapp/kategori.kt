package wulandari.afifah.uasapp

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.kategorilayout.*
import org.json.JSONArray
import org.json.JSONObject


class kategori  : AppCompatActivity() , View.OnClickListener {

    var id_kate : String =""
    var daftarJenis = mutableListOf<HashMap<String,String>>()
    var url = "http://192.168.43.126/buku/show_jenis.php"
    var url1 = "http://192.168.43.126/buku/query_jenis.php"
    lateinit var Adapter :Adapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.kategorilayout)
        Adapter = Adapter(daftarJenis,this)
        lisJenis.layoutManager = LinearLayoutManager(this)
        lisJenis.adapter = Adapter
        btin.setOnClickListener(this)
        bthap.setOnClickListener(this)
        btup.setOnClickListener(this)

    }

    override fun onStart() {
        super.onStart()
        Showprod()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btin->{
                query("insert")
            }
            R.id.btup->{
                query("update")
            }
            R.id.bthap->{
                query("delete")

            }
        }
    }


    fun Showprod(){
        val request = StringRequest(Request.Method.POST,url, Response.Listener { response ->
            daftarJenis.clear()
            Adapter.notifyDataSetChanged()
            val jsonArray = JSONArray(response)
            for (x in 0..(jsonArray.length()-1)){
                val jsonObject = jsonArray.getJSONObject(x)
                var jenis = HashMap<String,String>()
                jenis.put("id_jenis",jsonObject.getInt("id_jenis").toString())
                jenis.put("nm_jenis",jsonObject.getString("nm_jenis"))
                daftarJenis.add(jenis)
            }
            Adapter.notifyDataSetChanged()
        }, Response.ErrorListener { error ->
            Toast.makeText(this,"Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
        })
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }


    fun query(mode : String){
        val request = object : StringRequest(Method.POST,url1,
            Response.Listener { response ->
                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil ", Toast.LENGTH_LONG).show()
                    Showprod()
                    jnsBuku.setText("")
                }else{
                    Toast.makeText(this,"Operasi Gagal ", Toast.LENGTH_LONG).show()
                } },
            Response.ErrorListener { error ->
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                //var nmFile = "DC"+SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())+".jpg"  = > memberinama pada foto
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        // hm.put("id_type",id_typ)
                        hm.put("nm_jenis",jnsBuku.text.toString())


                        //hm.put("image",imStr)
                        //hm.put("file",namafile)
                        //hm.put("nama_prodi",pilihprodi)
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("id_jenis",id_kate)
                        hm.put("nm_jenis",jnsBuku.text.toString())
                        //  hm.put("nama",txNamaMhs.text.toString())
                        // hm.put("image",imStr)
                        //hm.put("file",namafile)
                        // hm.put("nama_prodi",pilihprodi)
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("id_jenis",id_kate)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }




}