package wulandari.afifah.uasapp

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONArray
import org.json.JSONObject
import wulandari.afifah.uasapp.MainActivity
import wulandari.afifah.uasapp.R

class login : AppCompatActivity(),View.OnClickListener {

    var warnakat = ""
    lateinit var pref : SharedPreferences
    var daftarkategori = mutableListOf<HashMap<String, String>>()
    var uri1 = "http://192.168.43.58/dashboard/uas_cb/login.php"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        pref = getSharedPreferences("a", Context.MODE_PRIVATE)
        warnakat = pref.getString("bl","").toString()
        warna()

        login.setOnClickListener(this)
    }
    fun warna(){

        if (warnakat=="kuning"){
            laylog.setBackgroundColor(Color.YELLOW)
        }else if(warnakat=="biru"){
            laylog.setBackgroundColor(Color.BLUE)
        }else if (warnakat=="putih"){
            laylog.setBackgroundColor(Color.WHITE)
            txHeader.setTextColor(Color.BLACK)
        }else if(warnakat=="hijau"){
            laylog.setBackgroundColor(Color.GREEN)
        }

    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.login->{
                //login("insert")
                val iny = Intent(this,MainActivity::class.java)
                startActivity(iny)
            }
        }
    }


    fun login(mode : String){
        val request = object : StringRequest(
            Method.POST, uri1,
            Response.Listener { response ->

                val jsonobject = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")) {
                    val inten = Intent(this, MainActivity::class.java)
                    startActivity(inten)
                    Toast.makeText(this, "welcome", Toast.LENGTH_LONG).show()

                } else {
                    editText.error = "password anda salah"
                    Toast.makeText(this, "login gagal ", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                when (mode) {
                    "insert" -> {
                        hm.put("mode", "login")
                        hm.put("saya", editText.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }



}
