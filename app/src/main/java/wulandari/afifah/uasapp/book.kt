package wulandari.afifah.uasapp

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap

class book : AppCompatActivity() , View.OnClickListener {

    lateinit var AdapJenis : ArrayAdapter<String>
    lateinit var AdapterBuku : AdapterBuku
    lateinit var UploadHelper : UploadHelper
    var daftarmerk = mutableListOf<String>()
    var url = "http://192.168.43.126/buku/show_jenis.php"
    var url1 = "http://192.168.43.126/buku/show_buku.php"
    var url2 = "http://192.168.43.126/buku/query_buku.php"
    var nm_jenis = ""
    var datainfo  = mutableListOf<HashMap<String,String>>()
    var imStr =""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        UploadHelper = UploadHelper(this)

        AdapterBuku = AdapterBuku(datainfo,this)
        Rcbook.layoutManager = LinearLayoutManager(this)
        Rcbook.adapter = AdapterBuku


        AdapJenis = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarmerk)
        SpProdi.adapter = AdapJenis
        SpProdi.onItemSelectedListener = itemselect


        btInsert.setOnClickListener(this)
        btUpdate.setOnClickListener(this)
        btDel.setOnClickListener(this)
        imageView2.setOnClickListener(this)
    }


    val itemselect = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            SpProdi.setSelection(0)
            nm_jenis = daftarmerk.get(0)

        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            nm_jenis = daftarmerk.get(position)


        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
           if(requestCode == UploadHelper.getRcGalery()){
                imStr = UploadHelper.getBitmapToString(data!!.data,imageView2)
            }
        }
    }


    override fun onStart() {
        super.onStart()
        showJenis()
        Showinfo("","")
    }


    fun Showinfo(namatype : String, namaMerk : String){
        val request = object : StringRequest(
            Request.Method.POST,url1, Response.Listener { response ->
                datainfo.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var info = HashMap<String,String>()
                    info.put("id_buku",jsonObject.getString("id_buku"))
                    info.put("judul_buku",jsonObject.getString("judul_buku"))
                    info.put("harga",jsonObject.getString("harga"))
                    info.put("nm_pengarang",jsonObject.getString("nm_pengarang"))
                    info.put("nm_jenis",jsonObject.getString("nm_jenis"))
                    info.put("url",jsonObject.getString("url"))

                    datainfo.add(info)
                }
                AdapterBuku.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
            }){ override fun getParams(): MutableMap<String, String> {
            val hm = HashMap<String,String>()
            hm.put("nama",namatype)
            hm.put("mek",namaMerk)
            return hm
        }
        }
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }




    fun showJenis(){
        val request  = StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarmerk.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarmerk.add(jsonObject.getString("nm_jenis"))
                }
                AdapJenis.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->

            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    override fun onClick(v: View?) {
      when(v?.id){
          R.id.btInsert->{
                query("insert")
          }

          R.id.btUpdate->{
            query("update")
          }
          R.id.btDel->{
              query("delete")
          }
          R.id.imageView2->{
              val intent  = Intent()
              intent.setType("image/*")
              intent.setAction(Intent.ACTION_GET_CONTENT)
              startActivityForResult(intent,UploadHelper.getRcGalery())
          }

      }
    }

    fun query(mode : String){
        val request = object : StringRequest(Method.POST,url2,
            Response.Listener { response ->
                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil ", Toast.LENGTH_LONG).show()
                    Showinfo("","")
                }else{
                    Toast.makeText(this,"Operasi Gagal ", Toast.LENGTH_LONG).show()
                } },
            Response.ErrorListener { error ->
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                var namafile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())+".jpg"
                when(mode){

                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("id_buku",txID.text.toString())
                        hm.put("judul_buku",txJDL.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",namafile)
                        hm.put("nm_jenis",nm_jenis)
                        hm.put("nm_pengarang",pengarang.text.toString())
                        hm.put("harga",harga.text.toString())
                    }

                    "update"->{
                        hm.put("mode","update")
                        hm.put("id_buku",txID.text.toString())
                        hm.put("judul_buku",txJDL.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",namafile)
                        hm.put("nm_jenis",nm_jenis)
                        hm.put("nm_pengarang",pengarang.text.toString())
                        hm.put("harga",harga.text.toString())
                    }

                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("id_buku",txID.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

}